

  ┌───────┐
  │       │
  │  a:o  │  acolono.com
  │       │
  └───────┘


INTRODUCTION
------------
This Module allows adding whatchadoo videos via tokens in text fields.


REQUIREMENTS
------------
This module extends the "Video Filter" contrib module,
see http://drupal.org/project/video_filter.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module.

See: https://drupal.org/documentation/install/modules-themes/modules-7
for further information.


CONFIGURATION
-------------

* Make sure video filter is enabled for your input formats at
admin/config/content/formats.
* Make sure "video filter" is after
"Limit allowed HTML tags" (if your are using it) to avoid problems.


USAGE EXAMPLE
-------------

[video:https://www.whatchado.com/de/some-title]
